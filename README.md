# parabint

A library for parabolic trajectory generation with velocity, acceleration, and minimum-switch-time constraints.

## Getting Started

Clone this repository via

```bash
git clone https://gitlab.com/puttichai/parabint.git
```

Then go to the root folder of parabint and install parabint by
```bash
sudo python setup.py install
```

## Examples

